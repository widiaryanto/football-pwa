var base_url = "https://api.football-data.org/v2/";

const API_KEY = '5395bb9919364fca8824d1831f7a076c';
const LEAGUE_ID = 2001

var standings_url = `${base_url}competitions/${LEAGUE_ID}/standings?standingType=TOTAL`
var matches_url = `${base_url}competitions/${LEAGUE_ID}/matches`
var teams_url = `${base_url}competitions/${LEAGUE_ID}/teams`

function fetchApi(url) {
  return fetch(url, {
    headers: {
      'X-Auth-Token': API_KEY
    }
  });
}

function status(response) {
	if (response.status !== 200) {
		console.log("Error : " + response.status);
		return Promise.reject(new Error(response.statusText));
	} else {
		return Promise.resolve(response);
	}
}

function json(response) {
	return response.json();
}

function error(error) {
	console.log("Error : " + error);
}

function loading() {
  var html = ` 
    <div>
      <img src="/img/loading.gif"/>
    </div>`
  document.getElementById("loading").innerHTML = html;
  document.getElementById("title-content").innerHTML = '';
  document.getElementById("main-content").innerHTML = '';
}

function hideload() {
  document.getElementById("loading").innerHTML = '';
}

function getStandings() {
  loading();
	fetchApi(standings_url)
	.then(status)
	.then(json)
	.then(function(data) {
    var html = '';
   	data.standings.forEach(function(standing) {
      var detail = '';
    	standing.table.forEach(function(result) {
     		detail += `
          <tr>
            <td>${result.position}</td>
            <td>${result.team.name}</td>
            <td>${result.playedGames}</td>
            <td>${result.won}</td>
            <td>${result.draw}</td>
            <td>${result.lost}</td>
            <td>${result.points}</td>
            <td>${result.goalsFor}</td>
            <td>${result.goalsAgainst}</td>
            <td>${result.goalDifference}</td>
          </tr>
        `
      });
      html += `
      	<div style="text-align: center">
       		<h5 class="header">${standing.group}</h5>
       	</div>
        <div class="col s12 m12">
	        <div class="card">
        		<div class="card-content">
       				<table class="striped">
       					<head>
	      					<tr>
           					<th>Position</th>
            				<th>Team</th>
            				<th>Played</th>
            				<th>Won</th>
           					<th>Draw</th>
           					<th>Lost</th>
           					<th>Points</th>
           					<th>GF</th>
           					<th>GA</th>
            				<th>GD</th>
          				</tr>
        				</head>
        				<body>` + detail + `</body>
        			</table>
        		</div>
       		</div>
       	</div>
      `
    });
    document.getElementById("title-content").innerHTML = 'Champions League';
    document.getElementById("main-content").innerHTML = html;
    hideload();
  })
	.catch(error);
}

function getMatches() {
  loading();
	fetchApi(matches_url)
	.then(status)
	.then(json)
	.then(function(data) {
    matchesData = data;
		var groupBy = function (xs, key) {
			return xs.reduce(function (rv, x) {
				(rv[x[key]] = rv[x[key]] || []).push(x);
    		return rv;
			}, {});
		};
		var matchdays = groupBy(data.matches, 'matchday');
    var html = '';
    for (const key in matchdays) {
    	if (key != 'null') {
       	html += `
          <div style="text-align: center">
            <h5 class="header">Group stage ${key} of 7</h5>
          </div>
          <div class="card">
            <div class="card-content">
              <div class="row ">
        `
        matchdays[key].forEach(function(match) {
          var dateToDMY = function(date) {
						return `${date.getDate()}-${date.getMonth() + 1}-${date.getFullYear()}`
          }
          html += `
            <div class="col s12 m6 l6" style="border-top:rgba(0,0,0,.2) solid 1px">
              <div style="text-align: center">
                <h6>${dateToDMY(new Date(match.utcDate))}</h6>
              </div>
              <div class="col s4 m4 l4">${match.homeTeam.name}</div>
              <div class="col s1">${match.score.fullTime.homeTeam}</div>
              <div style="text-align: center" class="col s2">vs</div>
              <div style="text-align: right" class="col s1">${match.score.fullTime.awayTeam}</div>
              <div style="text-align: right" class="col s4">${match.awayTeam.name}</div>
            </div>
          `
        });
        html += `
              </div>
            </div>
          </div>
        `
      }
    }
    document.getElementById("title-content").innerHTML = 'Champions League Matches';
    document.getElementById("main-content").innerHTML = html;
    hideload();
	})
	.catch(error);
}

function getTeams() {
  loading();
	fetchApi(teams_url)
	.then(status)
	.then(json)
	.then(function(data) {
    teamsData = data;
		var html = '';
		html += `<div class="row">`
		data.teams.forEach(function(team) {
      html += `
      	<div class="col s12 m6 l6">
        	<div class="card">
          	<div class="card-content">
           		<div class="center">
           			<img width="65" height="65" src="${team.crestUrl || '/img/not_found.jpg'}">
           		</div>
            	<div class="center flow-text">${team.name}</div>
            	<div class="center">${team.area.name}</div>
            	<div class="center"><a href="${team.website}">${team.website}</a></div>
          	</div>
        	</div>
      	</div>
    	`
    });
    html += `</div>`
    document.getElementById("title-content").innerHTML = 'Champions League Teams';
    document.getElementById("main-content").innerHTML = html;
    hideload();
	})
	.catch(error);
}

function privacyPolicy() {
  loading();
  var html = '';
  html += `
  <center>*****************************</center>
  <h5>Football Schedule (Jadwal Liga Dunia)</h5>
  <p>
    Muhamad Widi Aryanto built the Football Schedule (Jadwal Liga Dunia) app as a Free app. This SERVICE is provided by 
    Muhamad Widi Aryanto at no cost and is intended for use as is.
  </p>
  <p> 
    This page is used to inform visitors regarding my policies with the collection, use, and disclosure 
    of Personal Information if anyone decided to use my Service.
  </p> 
  <p> 
    If you choose to use my Service, then you agree to the collection and use of information in 
    relation to this policy. The Personal Information that I collect is used for providing and improving 
    the Service. I will not use or share your information with anyone except as described 
    in this Privacy Policy.
  </p> 
  <p> 
    The terms used in this Privacy Policy have the same meanings as in our Terms and Conditions, which is
    accessible at Football Schedule (Jadwal Liga Dunia) unless otherwise defined in this Privacy Policy.
  </p> 
  <p>
    <h5>Information Collection and Use</h5>
  </p> 
  <p> 
    For a better experience, while using our Service, I may require you to provide us with certain
    personally identifiable information. The information that I request will be retained on your device and is not collected by me in any way.
  </p> 
  <p> 
    The app does use third party services that may collect information used to identify you.</p> <div><p>Link to privacy policy of third party 
    service providers used by the app
  </p> 
  <ul>
    <li>
      <a href="https://www.google.com/policies/privacy/" target="_blank">Google Play Services</a>
    </li>
    <li>
      <a href="https://support.google.com/admob/answer/6128543?hl=en" target="_blank">AdMob</a>
    </li>
    <li>
      <a href="https://firebase.google.com/policies/analytics" target="_blank">Firebase Analytics</a>
    </li>
  </ul>
  <p>
    <h5>Log Data</h5>
  </p> 
  <p> 
    I want to inform you that whenever you use my Service, in a case of
    an error in the app I collect data and information (through third party products) on your phone
    called Log Data. This Log Data may include information such as your device Internet Protocol (“IP”) address,
    device name, operating system version, the configuration of the app when utilizing my Service,
    the time and date of your use of the Service, and other statistics.
  </p> 
  <p>
    <h5>Cookies</h5>
  </p> 
  <p>
    Cookies are files with a small amount of data that are commonly used as anonymous unique identifiers.
    These are sent to your browser from the websites that you visit and are stored on your device's internal
    memory.
  </p> 
  <p>
    This Service does not use these “cookies” explicitly. However, the app may use third party code and
    libraries that use “cookies” to collect information and improve their services. You have the option to
    either accept or refuse these cookies and know when a cookie is being sent to your device. If you choose
    to refuse our cookies, you may not be able to use some portions of this Service.
  </p> 
  <p>
    <h5>Service Providers</h5>
  </p> 
  <p> 
    I may employ third-party companies and individuals due to the following reasons:
  </p> 
  <ul>
    <li>
      To facilitate our Service;
    </li> 
    <li>
      To provide the Service on our behalf;
    </li> 
    <li>
      To perform Service-related services; or
    </li> 
    <li>
      To assist us in analyzing how our Service is used.
    </li>
  </ul> 
  <p> 
    I want to inform users of this Service that these third parties have access to
    your Personal Information. The reason is to perform the tasks assigned to them on our behalf. However,
    they are obligated not to disclose or use the information for any other purpose.
  </p> 
  <p>
    <h5>Security</h5>
  </p> 
  <p> 
    I value your trust in providing us your Personal Information, thus we are striving
    to use commercially acceptable means of protecting it. But remember that no method of transmission over
    the internet, or method of electronic storage is 100% secure and reliable, and I cannot guarantee
    its absolute security.
  </p> 
  <p>
    <h5>Links to Other Sites</h5>
  </p> 
  <p>
    This Service may contain links to other sites. If you click on a third-party link, you will be directed
    to that site. Note that these external sites are not operated by me. Therefore, I strongly
    advise you to review the Privacy Policy of these websites. I have no control over
    and assume no responsibility for the content, privacy policies, or practices of any third-party sites
    or services.
  </p> 
  <p>
    <h5>Children’s Privacy</h5>
  </p> 
  <p>
    These Services do not address anyone under the age of 13. I do not knowingly collect
    personally identifiable information from children under 13. In the case I discover that a child
    under 13 has provided me with personal information, I immediately delete this from
    our servers. If you are a parent or guardian and you are aware that your child has provided us with personal
    information, please contact me so that I will be able to do necessary actions.
  </p> 
  <p>
    <h5>Changes to This Privacy Policy</h5>
  </p> 
  <p> 
    I may update our Privacy Policy from time to time. Thus, you are advised to review
    this page periodically for any changes. I will notify you of any changes by posting
    the new Privacy Policy on this page. These changes are effective immediately after they are posted on
    this page.
  </p> 
  <p>
    <h5>Contact Us</h5>
  </p> 
  <p>
    If you have any questions or suggestions about my Privacy Policy, do not hesitate to contact me.
  </p>
  <p>
    <center>Copyright <strong>© 2019 Muhamad Widi Aryanto</strong></center>
  </p>`
  document.getElementById("title-content").innerHTML = 'Privacy Policy';
  document.getElementById("main-content").innerHTML = html;
  hideload();
}

function getTAC() {
  loading();
  var html = '';
  html += `
  <center>*****************************</center>
  <h5>Terms and conditions</h5>
  <p>
    These terms and conditions (&quot;Terms&quot;, &quot;Agreement&quot;) are an agreement between Mobile Application Developer (&quot;Mobile Application Developer&quot;, &quot;us&quot;, &quot;we&quot; or &quot;our&quot;) and you (&quot;User&quot;, &quot;you&quot; or &quot;your&quot;). This Agreement sets forth the general terms and conditions of your use of the Football Schedule (Jadwal Liga Dunia) mobile application and any of its products or services (collectively, &quot;Mobile Application&quot; or &quot;Services&quot;).
  </p>
  <h5>Backups</h5>
  <p>
    We are not responsible for Content residing in the Mobile Application. In no event shall we be held liable for any loss of any Content. It is your sole responsibility to maintain appropriate backup of your Content. Notwithstanding the foregoing, on some occasions and in certain circumstances, with absolutely no obligation, we may be able to restore some or all of your data that has been deleted as of a certain date and time when we may have backed up data for our own purposes. We make no guarantee that the data you need will be available.
  </p>
  <h5>Advertisements</h5>
  <p>
    During use of the Mobile Application, you may enter into correspondence with or participate in promotions of advertisers or sponsors showing their goods or services through the Mobile Application. Any such activity, and any terms, conditions, warranties or representations associated with such activity, is solely between you and the applicable third-party. We shall have no liability, obligation or responsibility for any such correspondence, purchase or promotion between you and any such third-party.
  </p>
  <h5>Indemnification</h5>
  <p>
    You agree to indemnify and hold Mobile Application Developer and its affiliates, directors, officers, employees, and agents harmless from and against any liabilities, losses, damages or costs, including reasonable attorneys' fees, incurred in connection with or arising from any third-party allegations, claims, actions, disputes, or demands asserted against any of them as a result of or relating to your Content, your use of the Mobile Application or Services or any willful misconduct on your part.
  </p>
  <h5>Changes and amendments</h5>
  <p>
    We reserve the right to modify this Agreement or its policies relating to the Mobile Application or Services at any time, effective upon posting of an updated version of this Agreement in the Mobile Application. When we do, we will revise the updated date at the bottom of this page. Continued use of the Mobile Application after any such changes shall constitute your consent to such changes.
  </p>
  <h5>Acceptance of these terms</h5>
  <p>
    You acknowledge that you have read this Agreement and agree to all its terms and conditions. By using the Mobile Application or its Services you agree to be bound by this Agreement. If you do not agree to abide by the terms of this Agreement, you are not authorized to use or access the Mobile Application and its Services.
  </p>
  <h5>Contacting us</h5>
  <p>
    If you have any questions about this Agreement, please contact us.
  </p>
  <p>
    <center>Copyright <strong>© 2019 Muhamad Widi Aryanto</strong></center>
  </p>`
  document.getElementById("title-content").innerHTML = 'Terms and Conditions';
  document.getElementById("main-content").innerHTML = html;
  hideload();
}